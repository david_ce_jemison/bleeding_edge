/*
 * Copyright (c) 2013, the Dart project authors.
 * 
 * Licensed under the Eclipse Public License v1.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.google.dart.compiler.metrics;

/**
 *
 */
public class CompilerMetrics {

  public int getJSNativeLibCharSize() {

    return 0;
  }

  public double getLinesPerMS() {

    return 0;
  }

  public double getNonCommentLinesPerMS() {

    return 0;
  }

  public char[] getNumCharsParsed() {

    return null;
  }

  public char[] getNumLinesParsed() {

    return null;
  }

  public char[] getNumNonCommentChars() {

    return null;
  }

  public char[] getNumNonCommentLines() {

    return null;
  }

  public char[] getNumUnitsParsed() {

    return null;
  }

  public char[] getParseTime() {

    return null;
  }

  public double getPercentCharsConsumedByNativeLibraries() {

    return 0;
  }

  public double getPercentTimeParsing() {

    return 0;
  }

  public double getTimeSpentPerUnit() {

    return 0;
  }

  public char[] getTotalCompilationTime() {

    return null;
  }

}
