A documentation generator for Dart.

The docgen tool takes in a library as input and produces documentation
for it as well as all libraries it imports and uses.

This outputs information about all classes, variables, functions, and
methods defined in the library and its imported libraries.

## USAGE

  dart docgen.dart path/to/file.dart